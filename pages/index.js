import Image from 'next/image'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <div class="flower-container">
      <div class="flower-leaf"></div>
      <div class="flower-stem"></div>
      <div class="flower-top">
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-petal"></div>
        <div class="flower-center"></div>
      </div>
      <div class="flower-text">Leucanthemum vulgare</div>
    </div>
  )
}
